﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ShermanInmuebles.Administrator.Startup))]
namespace ShermanInmuebles.Administrator
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
