namespace ShermanInmuebles.DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ShermanDataContext : DbContext
    {
        public ShermanDataContext()
            : base("name=ShermanContext")
        {
        }

        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<Asesor> Asesor { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<Cat_Colonia> Cat_Colonia { get; set; }
        public virtual DbSet<Cat_Country> Cat_Country { get; set; }
        public virtual DbSet<Cat_Locality> Cat_Locality { get; set; }
        public virtual DbSet<Cat_Municipality> Cat_Municipality { get; set; }
        public virtual DbSet<Cat_State> Cat_State { get; set; }
        public virtual DbSet<FotoInmueble> FotoInmueble { get; set; }
        public virtual DbSet<Inmobiliaria> Inmobiliaria { get; set; }
        public virtual DbSet<Inmueble> Inmueble { get; set; }
        public virtual DbSet<LoNuevo> LoNuevo { get; set; }
        public virtual DbSet<MensajeContacto> MensajeContacto { get; set; }
        public virtual DbSet<OrganizationType> OrganizationType { get; set; }
        public virtual DbSet<Properties> Properties { get; set; }
        public virtual DbSet<Propietario> Propietario { get; set; }
        public virtual DbSet<TipoInmueble> TipoInmueble { get; set; }
        public virtual DbSet<Usuario> Usuario { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Asesor>()
                .Property(e => e.estatus_asesor)
                .IsUnicode(false);

            modelBuilder.Entity<Asesor>()
                .Property(e => e.tipo_asesor)
                .IsUnicode(false);

            modelBuilder.Entity<Asesor>()
                .Property(e => e.nombre_asesor)
                .IsUnicode(false);

            modelBuilder.Entity<Asesor>()
                .Property(e => e.apellido_paterno_asesor)
                .IsUnicode(false);

            modelBuilder.Entity<Asesor>()
                .Property(e => e.apellido_materno_asesor)
                .IsUnicode(false);

            modelBuilder.Entity<Asesor>()
                .Property(e => e.usuario_asesor)
                .IsUnicode(false);

            modelBuilder.Entity<Asesor>()
                .Property(e => e.contrasena_asesor)
                .IsUnicode(false);

            modelBuilder.Entity<Asesor>()
                .Property(e => e.telefono1_asesor)
                .IsUnicode(false);

            modelBuilder.Entity<Asesor>()
                .Property(e => e.telefono2_asesor)
                .IsUnicode(false);

            modelBuilder.Entity<Asesor>()
                .Property(e => e.movil_asesor)
                .IsUnicode(false);

            modelBuilder.Entity<Asesor>()
                .Property(e => e.nextel_asesor)
                .IsUnicode(false);

            modelBuilder.Entity<Asesor>()
                .Property(e => e.nextel_radio_asesor)
                .IsUnicode(false);

            modelBuilder.Entity<Asesor>()
                .Property(e => e.email1_asesor)
                .IsUnicode(false);

            modelBuilder.Entity<Asesor>()
                .Property(e => e.email2_asesor)
                .IsUnicode(false);

            modelBuilder.Entity<Asesor>()
                .Property(e => e.domicilio_asesor)
                .IsUnicode(false);

            modelBuilder.Entity<Asesor>()
                .Property(e => e.entre_calles_asesor)
                .IsUnicode(false);

            modelBuilder.Entity<Asesor>()
                .Property(e => e.zona_asesor)
                .IsUnicode(false);

            modelBuilder.Entity<Asesor>()
                .Property(e => e.observaciones_asesor)
                .IsUnicode(false);

            modelBuilder.Entity<Asesor>()
                .Property(e => e.foto_asesor)
                .IsUnicode(false);

            modelBuilder.Entity<AspNetRoles>()
                .HasMany(e => e.AspNetUsers)
                .WithMany(e => e.AspNetRoles)
                .Map(m => m.ToTable("AspNetUserRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AspNetUserClaims)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AspNetUserLogins)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<Cat_Colonia>()
                .Property(e => e.nombre_colonia)
                .IsUnicode(false);

            modelBuilder.Entity<Cat_Colonia>()
                .Property(e => e.cp_colonia)
                .IsFixedLength();

            modelBuilder.Entity<Cat_Colonia>()
                .HasMany(e => e.Asesor)
                .WithRequired(e => e.Cat_Colonia)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cat_Country>()
                .Property(e => e.Country)
                .IsUnicode(false);

            modelBuilder.Entity<Cat_Country>()
                .HasMany(e => e.Cat_State)
                .WithRequired(e => e.Cat_Country)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cat_Locality>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<Cat_Locality>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Cat_Locality>()
                .Property(e => e.Lat)
                .IsUnicode(false);

            modelBuilder.Entity<Cat_Locality>()
                .Property(e => e.Lon)
                .IsUnicode(false);

            modelBuilder.Entity<Cat_Locality>()
                .Property(e => e.Latitude)
                .HasPrecision(10, 7);

            modelBuilder.Entity<Cat_Locality>()
                .Property(e => e.Longitude)
                .HasPrecision(10, 7);

            modelBuilder.Entity<Cat_Locality>()
                .Property(e => e.Altitude)
                .IsUnicode(false);

            modelBuilder.Entity<Cat_Municipality>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<Cat_Municipality>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Cat_Municipality>()
                .HasMany(e => e.Cat_Locality)
                .WithRequired(e => e.Cat_Municipality)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cat_State>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<Cat_State>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Cat_State>()
                .Property(e => e.Abbreviation)
                .IsUnicode(false);

            modelBuilder.Entity<Cat_State>()
                .HasMany(e => e.Cat_Municipality)
                .WithRequired(e => e.Cat_State)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<FotoInmueble>()
                .Property(e => e.nombre_fotoinmueble)
                .IsUnicode(false);

            modelBuilder.Entity<FotoInmueble>()
                .Property(e => e.descripcion_fotoinmueble)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.clave_ampi_nacional)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.estatus_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.nombre_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.razon_social_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.rfc_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.representante_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.contacto_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.domicilio_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.zona_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.entre_calles_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.referencia_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.telefono1_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.telefono2_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.telefono3_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.email1_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.email2_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.sitio_web_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.plantilla_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.numero_plantilla_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.eslogan_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.quienes_somos_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.nosotros_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.observaciones_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.logotipo_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.foto_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.inmobiliaria_ampi)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.inmobiliaria_solocasas)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.lat_mapa_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.lng_mapa_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .Property(e => e.zoom_mapa_inmobiliaria)
                .IsUnicode(false);

            modelBuilder.Entity<Inmobiliaria>()
                .HasMany(e => e.Asesor)
                .WithRequired(e => e.Inmobiliaria)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.promocion_inmueble)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.clave_catastral_inmueble)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.uso_inmueble)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.estatus_inmueble)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.comentarios_estatus_inmueble)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.ano_construccion_inmueble)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.estado_inmueble)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.unidad_superficie_inmueble)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.precio_inmueble)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.moneda_inmueble)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.publicar_domicilio_inmueble)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.domicilio_inmueble)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.numero_ext_inmueble)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.numero_int_inmueble)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.entre_calles_inmueble)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.zona_inmueble)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.referencia1_inmueble)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.referencia2_inmueble)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.ampi)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.especificaciones_inmueble)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.comentarios_inmueble)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.observaciones_inmueble)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.videoyoutube1_inmueble)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.videoyoutube2_inmueble)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.videoyoutube3_inmueble)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.lat_mapa)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.lng_mapa)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.zoom_mapa)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.tipo_mapa)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.lat_sv)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.lng_sv)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.zoom_sv)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.yaw_sv)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.pitch_sv)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .Property(e => e.total_fotos)
                .IsUnicode(false);

            modelBuilder.Entity<Inmueble>()
                .HasMany(e => e.FotoInmueble)
                .WithRequired(e => e.Inmueble)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LoNuevo>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<LoNuevo>()
                .Property(e => e.descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<LoNuevo>()
                .Property(e => e.href)
                .IsUnicode(false);

            modelBuilder.Entity<MensajeContacto>()
                .Property(e => e.nombre_mensajecontacto)
                .IsUnicode(false);

            modelBuilder.Entity<MensajeContacto>()
                .Property(e => e.telefono_mensajecontacto)
                .IsUnicode(false);

            modelBuilder.Entity<MensajeContacto>()
                .Property(e => e.email_mensajecontacto)
                .IsUnicode(false);

            modelBuilder.Entity<MensajeContacto>()
                .Property(e => e.comentario_mensajecontacto)
                .IsUnicode(false);

            modelBuilder.Entity<MensajeContacto>()
                .Property(e => e.estatus_mensajecontacto)
                .IsUnicode(false);

            modelBuilder.Entity<OrganizationType>()
                .Property(e => e.OrganizationType1)
                .IsUnicode(false);

            modelBuilder.Entity<Propietario>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Propietario>()
                .Property(e => e.apellidoPaterno)
                .IsUnicode(false);

            modelBuilder.Entity<Propietario>()
                .Property(e => e.apellidoMaterno)
                .IsUnicode(false);

            modelBuilder.Entity<Propietario>()
                .Property(e => e.telefono1)
                .IsUnicode(false);

            modelBuilder.Entity<Propietario>()
                .Property(e => e.telefono2)
                .IsUnicode(false);

            modelBuilder.Entity<Propietario>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<Propietario>()
                .Property(e => e.domicilio)
                .IsUnicode(false);

            modelBuilder.Entity<Propietario>()
                .HasMany(e => e.Asesor)
                .WithRequired(e => e.Propietario)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Propietario>()
                .HasMany(e => e.Inmueble)
                .WithRequired(e => e.Propietario)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TipoInmueble>()
                .Property(e => e.nombre_tipoinmueble)
                .IsUnicode(false);

            modelBuilder.Entity<TipoInmueble>()
                .HasMany(e => e.Inmueble)
                .WithRequired(e => e.TipoInmueble)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.estatus_usuario)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.nombre_usuario)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.puesto_usuario)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.tipo_usuario)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.email_usuario)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.login_usuario)
                .IsUnicode(false);

            modelBuilder.Entity<Usuario>()
                .Property(e => e.password_usuario)
                .IsUnicode(false);
        }
    }
}
