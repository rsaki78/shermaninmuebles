namespace ShermanInmuebles.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FotoInmueble")]
    public partial class FotoInmueble
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idfotoinmueble { get; set; }

        public int idinmueble { get; set; }

        public short numero_fotoinmueble { get; set; }

        [StringLength(50)]
        public string nombre_fotoinmueble { get; set; }

        [StringLength(50)]
        public string descripcion_fotoinmueble { get; set; }

        public bool isActive { get; set; }

        public virtual Inmueble Inmueble { get; set; }
    }
}
