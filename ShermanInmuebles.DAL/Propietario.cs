namespace ShermanInmuebles.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Propietario")]
    public partial class Propietario
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Propietario()
        {
            Asesor = new HashSet<Asesor>();
            Inmueble = new HashSet<Inmueble>();
        }

        [Key]
        public int idpropietario { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name ="Name")]
        public string nombre { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Miden Name")]
        public string apellidoPaterno { get; set; }

        [StringLength(50)]
        [Display(Name = "Last Name")]
        public string apellidoMaterno { get; set; }

        [Required]
        [StringLength(10)]
        [Display(Name = "Contact Phone")]
        public string telefono1 { get; set; }

        [StringLength(10)]
        [Display(Name = "Cell Phone")]
        public string telefono2 { get; set; }

        [Required]
        [StringLength(90)]
        [Display(Name = "Email")]
        public string email { get; set; }

        [StringLength(255)]
        [Display(Name = "Address")]
        public string domicilio { get; set; }

        public bool isActive { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Asesor> Asesor { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Inmueble> Inmueble { get; set; }
    }
}
