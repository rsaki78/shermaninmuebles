namespace ShermanInmuebles.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MensajeContacto")]
    public partial class MensajeContacto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idmensajecontacto { get; set; }

        public int idinmueble { get; set; }

        public int idmunicipio { get; set; }

        public DateTime fecha_mensajecontacto { get; set; }

        [StringLength(50)]
        public string nombre_mensajecontacto { get; set; }

        [StringLength(10)]
        public string telefono_mensajecontacto { get; set; }

        [StringLength(70)]
        public string email_mensajecontacto { get; set; }

        [StringLength(70)]
        public string comentario_mensajecontacto { get; set; }

        [StringLength(70)]
        public string estatus_mensajecontacto { get; set; }

        public bool isActive { get; set; }
    }
}
