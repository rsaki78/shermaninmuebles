namespace ShermanInmuebles.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Inmueble")]
    public partial class Inmueble
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Inmueble()
        {
            FotoInmueble = new HashSet<FotoInmueble>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idinmueble { get; set; }

        public int idpropietario { get; set; }

        public int idtipoinmueble { get; set; }

        public int idcolonia { get; set; }

        public DateTime? fecha_registro_inmueble { get; set; }

        public DateTime? fecha_baja_inmueble { get; set; }

        public DateTime? fecha_inicio_inmueble { get; set; }

        public DateTime? fecha_fin_inmueble { get; set; }

        public DateTime? fecha_notificacion { get; set; }

        public DateTime? fecha_vigencia { get; set; }

        [StringLength(50)]
        public string promocion_inmueble { get; set; }

        [StringLength(50)]
        public string clave_catastral_inmueble { get; set; }

        [StringLength(50)]
        public string uso_inmueble { get; set; }

        [StringLength(50)]
        public string estatus_inmueble { get; set; }

        [StringLength(150)]
        public string comentarios_estatus_inmueble { get; set; }

        [StringLength(150)]
        public string ano_construccion_inmueble { get; set; }

        [StringLength(70)]
        public string estado_inmueble { get; set; }

        [StringLength(50)]
        public string unidad_superficie_inmueble { get; set; }

        [StringLength(50)]
        public string precio_inmueble { get; set; }

        [StringLength(90)]
        public string moneda_inmueble { get; set; }

        [StringLength(150)]
        public string publicar_domicilio_inmueble { get; set; }

        [StringLength(50)]
        public string domicilio_inmueble { get; set; }

        [StringLength(50)]
        public string numero_ext_inmueble { get; set; }

        [StringLength(50)]
        public string numero_int_inmueble { get; set; }

        [StringLength(50)]
        public string entre_calles_inmueble { get; set; }

        [StringLength(50)]
        public string zona_inmueble { get; set; }

        [StringLength(50)]
        public string referencia1_inmueble { get; set; }

        [StringLength(50)]
        public string referencia2_inmueble { get; set; }

        [StringLength(50)]
        public string ampi { get; set; }

        public int? visitas_inmueble { get; set; }

        public int? visitas_busqueda { get; set; }

        [StringLength(200)]
        public string especificaciones_inmueble { get; set; }

        [StringLength(200)]
        public string comentarios_inmueble { get; set; }

        [StringLength(200)]
        public string observaciones_inmueble { get; set; }

        [StringLength(200)]
        public string videoyoutube1_inmueble { get; set; }

        [StringLength(200)]
        public string videoyoutube2_inmueble { get; set; }

        [StringLength(200)]
        public string videoyoutube3_inmueble { get; set; }

        public short? num_estacionamientos_inmueble { get; set; }

        public short? num_recamaras_inmueble { get; set; }

        public byte? num_banos_inmueble { get; set; }

        public byte? num_medios_banos_inmueble { get; set; }

        public short? num_cuartos_inmueble { get; set; }

        public byte? niveles_construidos_inmueble { get; set; }

        [StringLength(50)]
        public string lat_mapa { get; set; }

        [StringLength(50)]
        public string lng_mapa { get; set; }

        [StringLength(50)]
        public string zoom_mapa { get; set; }

        [StringLength(50)]
        public string tipo_mapa { get; set; }

        [StringLength(50)]
        public string lat_sv { get; set; }

        [StringLength(50)]
        public string lng_sv { get; set; }

        [StringLength(50)]
        public string zoom_sv { get; set; }

        [StringLength(50)]
        public string yaw_sv { get; set; }

        [StringLength(50)]
        public string pitch_sv { get; set; }

        [StringLength(50)]
        public string total_fotos { get; set; }

        public bool isActive { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FotoInmueble> FotoInmueble { get; set; }

        public virtual Propietario Propietario { get; set; }

        public virtual TipoInmueble TipoInmueble { get; set; }
    }
}
