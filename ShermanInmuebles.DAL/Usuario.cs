namespace ShermanInmuebles.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Usuario")]
    public partial class Usuario
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idusuario { get; set; }

        public int idorganizacion { get; set; }

        public DateTime? fecha_registro_usuario { get; set; }

        [StringLength(50)]
        public string estatus_usuario { get; set; }

        [StringLength(70)]
        public string nombre_usuario { get; set; }

        [StringLength(50)]
        public string puesto_usuario { get; set; }

        [StringLength(50)]
        public string tipo_usuario { get; set; }

        [StringLength(90)]
        public string email_usuario { get; set; }

        [Required]
        [StringLength(50)]
        public string login_usuario { get; set; }

        [Required]
        [StringLength(50)]
        public string password_usuario { get; set; }

        public DateTime? ultimo_acceso_usuario { get; set; }

        public bool isActive { get; set; }
    }
}
