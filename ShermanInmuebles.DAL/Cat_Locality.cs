namespace ShermanInmuebles.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Cat_Locality
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LocalityId { get; set; }

        public int MunicipalityId { get; set; }

        [Required]
        [StringLength(10)]
        public string Code { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(15)]
        public string Lat { get; set; }

        [StringLength(15)]
        public string Lon { get; set; }

        public decimal? Latitude { get; set; }

        public decimal? Longitude { get; set; }

        [StringLength(15)]
        public string Altitude { get; set; }

        public bool isActive { get; set; }

        public virtual Cat_Municipality Cat_Municipality { get; set; }
    }
}
