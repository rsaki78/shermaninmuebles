namespace ShermanInmuebles.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Cat_Colonia
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Cat_Colonia()
        {
            Asesor = new HashSet<Asesor>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idcolonia { get; set; }

        public int idmunicipio { get; set; }

        [Required]
        [StringLength(100)]
        public string nombre_colonia { get; set; }

        [Required]
        [StringLength(5)]
        public string cp_colonia { get; set; }

        public bool isActive { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Asesor> Asesor { get; set; }
    }
}
