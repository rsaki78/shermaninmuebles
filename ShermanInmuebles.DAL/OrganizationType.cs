namespace ShermanInmuebles.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OrganizationType")]
    public partial class OrganizationType
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int OrganizationTypeId { get; set; }

        [Column("OrganizationType")]
        [Required]
        [StringLength(60)]
        public string OrganizationType1 { get; set; }

        public bool isActive { get; set; }
    }
}
