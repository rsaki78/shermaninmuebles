namespace ShermanInmuebles.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LoNuevo")]
    public partial class LoNuevo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idlonuevo { get; set; }

        [StringLength(50)]
        public string nombre { get; set; }

        [StringLength(150)]
        public string descripcion { get; set; }

        [StringLength(150)]
        public string href { get; set; }

        public bool isActive { get; set; }
    }
}
