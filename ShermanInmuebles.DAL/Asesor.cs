namespace ShermanInmuebles.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Asesor")]
    public partial class Asesor
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idasesor { get; set; }

        public int idpropietario { get; set; }

        public int idinmobiliaria { get; set; }

        public int idcolonia { get; set; }

        public DateTime? fecha_registro_asesor { get; set; }

        [StringLength(50)]
        public string estatus_asesor { get; set; }

        [StringLength(50)]
        public string tipo_asesor { get; set; }

        [StringLength(50)]
        public string nombre_asesor { get; set; }

        [StringLength(50)]
        public string apellido_paterno_asesor { get; set; }

        [StringLength(50)]
        public string apellido_materno_asesor { get; set; }

        [StringLength(150)]
        public string usuario_asesor { get; set; }

        [StringLength(70)]
        public string contrasena_asesor { get; set; }

        [StringLength(10)]
        public string telefono1_asesor { get; set; }

        [StringLength(10)]
        public string telefono2_asesor { get; set; }

        [StringLength(10)]
        public string movil_asesor { get; set; }

        [StringLength(10)]
        public string nextel_asesor { get; set; }

        [StringLength(15)]
        public string nextel_radio_asesor { get; set; }

        [StringLength(90)]
        public string email1_asesor { get; set; }

        [StringLength(90)]
        public string email2_asesor { get; set; }

        [StringLength(90)]
        public string domicilio_asesor { get; set; }

        [StringLength(70)]
        public string entre_calles_asesor { get; set; }

        [StringLength(50)]
        public string zona_asesor { get; set; }

        [StringLength(50)]
        public string observaciones_asesor { get; set; }

        [StringLength(150)]
        public string foto_asesor { get; set; }

        public DateTime? ultimo_acceso_asesor { get; set; }

        public virtual Cat_Colonia Cat_Colonia { get; set; }

        public virtual Inmobiliaria Inmobiliaria { get; set; }

        public virtual Propietario Propietario { get; set; }
    }
}
