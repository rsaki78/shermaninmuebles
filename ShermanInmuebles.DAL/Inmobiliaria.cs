namespace ShermanInmuebles.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Inmobiliaria")]
    public partial class Inmobiliaria
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Inmobiliaria()
        {
            Asesor = new HashSet<Asesor>();
        }

        [Key]
        public int idinmobiliaria { get; set; }

        public int? idcolonia { get; set; }

        public int? idpaqueteinmobiliaria { get; set; }

        [StringLength(100)]
        public string clave_ampi_nacional { get; set; }

        public DateTime? fecha_registro_inmobiliaria { get; set; }

        [StringLength(100)]
        public string estatus_inmobiliaria { get; set; }

        [StringLength(100)]
        public string nombre_inmobiliaria { get; set; }

        [StringLength(100)]
        public string razon_social_inmobiliaria { get; set; }

        [StringLength(100)]
        public string rfc_inmobiliaria { get; set; }

        [StringLength(100)]
        public string representante_inmobiliaria { get; set; }

        [StringLength(100)]
        public string contacto_inmobiliaria { get; set; }

        [StringLength(100)]
        public string domicilio_inmobiliaria { get; set; }

        [StringLength(100)]
        public string zona_inmobiliaria { get; set; }

        [StringLength(100)]
        public string entre_calles_inmobiliaria { get; set; }

        [StringLength(100)]
        public string referencia_inmobiliaria { get; set; }

        [StringLength(100)]
        public string telefono1_inmobiliaria { get; set; }

        [StringLength(100)]
        public string telefono2_inmobiliaria { get; set; }

        [StringLength(100)]
        public string telefono3_inmobiliaria { get; set; }

        [StringLength(100)]
        public string email1_inmobiliaria { get; set; }

        [StringLength(100)]
        public string email2_inmobiliaria { get; set; }

        [StringLength(100)]
        public string sitio_web_inmobiliaria { get; set; }

        [StringLength(100)]
        public string plantilla_inmobiliaria { get; set; }

        [StringLength(100)]
        public string numero_plantilla_inmobiliaria { get; set; }

        [StringLength(100)]
        public string eslogan_inmobiliaria { get; set; }

        [StringLength(100)]
        public string quienes_somos_inmobiliaria { get; set; }

        [StringLength(100)]
        public string nosotros_inmobiliaria { get; set; }

        [StringLength(100)]
        public string observaciones_inmobiliaria { get; set; }

        [StringLength(100)]
        public string logotipo_inmobiliaria { get; set; }

        [StringLength(100)]
        public string foto_inmobiliaria { get; set; }

        [StringLength(100)]
        public string inmobiliaria_ampi { get; set; }

        [StringLength(100)]
        public string inmobiliaria_solocasas { get; set; }

        [StringLength(100)]
        public string lat_mapa_inmobiliaria { get; set; }

        [StringLength(100)]
        public string lng_mapa_inmobiliaria { get; set; }

        [StringLength(100)]
        public string zoom_mapa_inmobiliaria { get; set; }

        public bool isActive { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Asesor> Asesor { get; set; }
    }
}
