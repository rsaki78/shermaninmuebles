﻿using System.Web;
using System.Web.Optimization;

namespace ShermanInmuebles.WebAPI
{
    public class BundleConfig
    {
        // Para obtener más información sobre Bundles, visite http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
            // preparado para la producción y podrá utilizar la herramienta de compilación disponible en http://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Styles").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/site.css",
                      "~/Content/loading-bar.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/social-buttons.css",
                      "~/Content/common.css"));


            /*
            <link href="content/css/bootstrap.min.css" rel="stylesheet" />
            <link href="content/css/site.css" rel="stylesheet" />
            <link href="content/css/loading-bar.css" rel="stylesheet" />
            <link href="content/css/font-awesome.min.css" rel="stylesheet" />
            <link href="content/css/social-buttons.css" rel="stylesheet" />
            <link href="content/css/common.css" rel="stylesheet" />
             */
        }
    }
}
