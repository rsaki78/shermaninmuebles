﻿using System.Data.Entity;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using ShermanInmuebles.DAL;

namespace ShermanInmuebles.WebAPI.Controllers
{
    public class InmueblesController : Controller
    {
        private ShermanDataContext db = new ShermanDataContext();

        // GET: Inmuebles
        public async Task<ActionResult> Index()
        {
            var inmueble = db.Inmueble.Include(i => i.Propietario).Include(i => i.TipoInmueble);
            return View(await inmueble.ToListAsync());
        }

        // GET: Inmuebles/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inmueble inmueble = await db.Inmueble.FindAsync(id);
            if (inmueble == null)
            {
                return HttpNotFound();
            }
            return View(inmueble);
        }

        // GET: Inmuebles/Create
        public ActionResult Create()
        {
            ViewBag.idpropietario = new SelectList(db.Propietario, "idpropietario", "nombre");
            ViewBag.idtipoinmueble = new SelectList(db.TipoInmueble, "idtipoinmueble", "nombre_tipoinmueble");
            return View();
        }

        // POST: Inmuebles/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "idinmueble,idpropietario,idtipoinmueble,idcolonia,fecha_registro_inmueble,fecha_baja_inmueble,fecha_inicio_inmueble,fecha_fin_inmueble,fecha_notificacion,fecha_vigencia,promocion_inmueble,clave_catastral_inmueble,uso_inmueble,estatus_inmueble,comentarios_estatus_inmueble,ano_construccion_inmueble,estado_inmueble,unidad_superficie_inmueble,precio_inmueble,moneda_inmueble,publicar_domicilio_inmueble,domicilio_inmueble,numero_ext_inmueble,numero_int_inmueble,entre_calles_inmueble,zona_inmueble,referencia1_inmueble,referencia2_inmueble,ampi,visitas_inmueble,visitas_busqueda,especificaciones_inmueble,comentarios_inmueble,observaciones_inmueble,videoyoutube1_inmueble,videoyoutube2_inmueble,videoyoutube3_inmueble,num_estacionamientos_inmueble,num_recamaras_inmueble,num_banos_inmueble,num_medios_banos_inmueble,num_cuartos_inmueble,niveles_construidos_inmueble,lat_mapa,lng_mapa,zoom_mapa,tipo_mapa,lat_sv,lng_sv,zoom_sv,yaw_sv,pitch_sv,total_fotos,isActive")] Inmueble inmueble)
        {
            if (ModelState.IsValid)
            {
                db.Inmueble.Add(inmueble);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.idpropietario = new SelectList(db.Propietario, "idpropietario", "nombre", inmueble.idpropietario);
            ViewBag.idtipoinmueble = new SelectList(db.TipoInmueble, "idtipoinmueble", "nombre_tipoinmueble", inmueble.idtipoinmueble);
            return View(inmueble);
        }

        // GET: Inmuebles/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inmueble inmueble = await db.Inmueble.FindAsync(id);
            if (inmueble == null)
            {
                return HttpNotFound();
            }
            ViewBag.idpropietario = new SelectList(db.Propietario, "idpropietario", "nombre", inmueble.idpropietario);
            ViewBag.idtipoinmueble = new SelectList(db.TipoInmueble, "idtipoinmueble", "nombre_tipoinmueble", inmueble.idtipoinmueble);
            return View(inmueble);
        }

        // POST: Inmuebles/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "idinmueble,idpropietario,idtipoinmueble,idcolonia,fecha_registro_inmueble,fecha_baja_inmueble,fecha_inicio_inmueble,fecha_fin_inmueble,fecha_notificacion,fecha_vigencia,promocion_inmueble,clave_catastral_inmueble,uso_inmueble,estatus_inmueble,comentarios_estatus_inmueble,ano_construccion_inmueble,estado_inmueble,unidad_superficie_inmueble,precio_inmueble,moneda_inmueble,publicar_domicilio_inmueble,domicilio_inmueble,numero_ext_inmueble,numero_int_inmueble,entre_calles_inmueble,zona_inmueble,referencia1_inmueble,referencia2_inmueble,ampi,visitas_inmueble,visitas_busqueda,especificaciones_inmueble,comentarios_inmueble,observaciones_inmueble,videoyoutube1_inmueble,videoyoutube2_inmueble,videoyoutube3_inmueble,num_estacionamientos_inmueble,num_recamaras_inmueble,num_banos_inmueble,num_medios_banos_inmueble,num_cuartos_inmueble,niveles_construidos_inmueble,lat_mapa,lng_mapa,zoom_mapa,tipo_mapa,lat_sv,lng_sv,zoom_sv,yaw_sv,pitch_sv,total_fotos,isActive")] Inmueble inmueble)
        {
            if (ModelState.IsValid)
            {
                db.Entry(inmueble).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.idpropietario = new SelectList(db.Propietario, "idpropietario", "nombre", inmueble.idpropietario);
            ViewBag.idtipoinmueble = new SelectList(db.TipoInmueble, "idtipoinmueble", "nombre_tipoinmueble", inmueble.idtipoinmueble);
            return View(inmueble);
        }

        // GET: Inmuebles/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inmueble inmueble = await db.Inmueble.FindAsync(id);
            if (inmueble == null)
            {
                return HttpNotFound();
            }
            return View(inmueble);
        }

        // POST: Inmuebles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Inmueble inmueble = await db.Inmueble.FindAsync(id);
            db.Inmueble.Remove(inmueble);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
