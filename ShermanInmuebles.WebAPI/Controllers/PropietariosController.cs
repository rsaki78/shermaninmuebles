﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShermanInmuebles.DAL;

namespace ShermanInmuebles.WebAPI.Controllers
{
    public class PropietariosController : Controller
    {
        private ShermanDataContext db = new ShermanDataContext();

        // GET: Propietarios
        public async Task<ActionResult> Index()
        {
            return View(await db.Propietario.ToListAsync());
        }

        // GET: Propietarios/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Propietario propietario = await db.Propietario.FindAsync(id);
            if (propietario == null)
            {
                return HttpNotFound();
            }

            propietario.telefono2 = string.IsNullOrEmpty(propietario.telefono2) ? propietario.telefono2 = string.Empty : propietario.telefono2;
            return View(propietario);
        }

        // GET: Propietarios/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Propietarios/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "idpropietario,nombre,apellidoPaterno,apellidoMaterno,telefono1,telefono2,email,domicilio")] Propietario propietario)
        {
            if (ModelState.IsValid)
            {
                propietario.isActive = true;
                db.Propietario.Add(propietario);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(propietario);
        }

        // GET: Propietarios/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Propietario propietario = await db.Propietario.FindAsync(id);
            if (propietario == null)
            {
                return HttpNotFound();
            }
            return View(propietario);
        }

        // POST: Propietarios/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "idpropietario,nombre,apellidoPaterno,apellidoMaterno,telefono1,telefono2,email,domicilio")] Propietario propietario)
        {
            if (ModelState.IsValid)
            {
                propietario.isActive = true;
                db.Entry(propietario).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(propietario);
        }

        // GET: Propietarios/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Propietario propietario = await db.Propietario.FindAsync(id);
            if (propietario == null)
            {
                return HttpNotFound();
            }
            return View(propietario);
        }

        // POST: Propietarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Propietario propietario = await db.Propietario.FindAsync(id);
            propietario.isActive = false;
            //db.Propietario.Remove(propietario);
            db.Entry(propietario).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
