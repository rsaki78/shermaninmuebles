﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ShermanInmuebles.WebAPI.Startup))]
namespace ShermanInmuebles.WebAPI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
